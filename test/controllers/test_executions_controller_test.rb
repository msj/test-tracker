require 'test_helper'

class TestExecutionsControllerTest < ActionController::TestCase
  setup do
    @test_execution = test_executions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:test_executions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create test_execution" do
    assert_difference('TestExecution.count') do
      post :create, test_execution: { date_executed: @test_execution.date_executed }
    end

    assert_redirected_to test_execution_path(assigns(:test_execution))
  end

  test "should show test_execution" do
    get :show, id: @test_execution
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @test_execution
    assert_response :success
  end

  test "should update test_execution" do
    patch :update, id: @test_execution, test_execution: { date_executed: @test_execution.date_executed }
    assert_redirected_to test_execution_path(assigns(:test_execution))
  end

  test "should destroy test_execution" do
    assert_difference('TestExecution.count', -1) do
      delete :destroy, id: @test_execution
    end

    assert_redirected_to test_executions_path
  end
end
