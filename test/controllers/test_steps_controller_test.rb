require 'test_helper'

class TestStepsControllerTest < ActionController::TestCase
  setup do
    @test_step = test_steps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:test_steps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create test_step" do
    assert_difference('TestStep.count') do
      post :create, test_step: {  }
    end

    assert_redirected_to test_step_path(assigns(:test_step))
  end

  test "should show test_step" do
    get :show, id: @test_step
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @test_step
    assert_response :success
  end

  test "should update test_step" do
    patch :update, id: @test_step, test_step: {  }
    assert_redirected_to test_step_path(assigns(:test_step))
  end

  test "should destroy test_step" do
    assert_difference('TestStep.count', -1) do
      delete :destroy, id: @test_step
    end

    assert_redirected_to test_steps_path
  end
end
