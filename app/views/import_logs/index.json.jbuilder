json.array!(@import_logs) do |import_log|
  json.extract! import_log, :id, :import_date, :file_name, :file_size
  json.url import_log_url(import_log, format: :json)
end
