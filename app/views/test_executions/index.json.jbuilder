json.array!(@test_executions) do |test_execution|
  json.extract! test_execution, :id, :date_executed
  json.url test_execution_url(test_execution, format: :json)
end
