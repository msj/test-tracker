require 'roo'
require 'date'

class ImportLog < ActiveRecord::Base
  def self.import(file)

    log = ImportLog.new
    log.import_date = Time.now 
    log.file_name = File.basename(file.path)
    log.save()


    ss = Roo::Excelx.new(file.path, nil, :ignore)
    ss.sheets.each_with_index do |sheet_name, index|

      #load/create the use case
      current_use_case = UseCase.new #load from db by the name
      current_use_case.name = sheet_name
      current_use_case.save()
  
      #load/create the test execution
      current_test_execution = TestExecution.new
      current_test_execution.date_executed = Date.parse(ss.sheet(index).row(1)[2].to_s)
      current_test_execution.save()
    
      #populate the test cases
      current_test_case = nil
      inside_steps = false
      2.upto(ss.sheet(index).last_row).each do |row_index|
        #find out if we are starting a new test case
        if(ss.sheet(index).row(row_index)[0].to_s.include?("Test Case ID"))
          inside_steps = false
          current_test_case = TestCase.new
          current_test_case.use_case = current_use_case
          current_test_case.case_id = ss.sheet(index).row(row_index)[1]
          puts "test case found"
        elsif(ss.sheet(index).row(row_index)[0].to_s.include?("Test Case Title"))
         current_test_case.case_title = ss.sheet(index).row(row_index)[1]
         #current_test_case.save()
        elsif(ss.sheet(index).row(row_index)[0].to_s.include?("Steps"))
          inside_steps = true
        end
       
        if(inside_steps && ss.sheet(index).row(row_index)[1].to_s != "")
          step = TestStep.new
          step.test_case = current_test_case
          step.step_name = ss.sheet(index).row(row_index)[1].to_s
          step.save()
   
          result = TestResult.new
          result.test_execution = current_test_execution
          result.test_step = step
          result.status = ss.sheet(index).row(row_index)[2].to_s == 'Y'
          result.save()
        end
      end

    end
  end
end
