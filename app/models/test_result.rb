class TestResult < ActiveRecord::Base
  belongs_to :test_step
  belongs_to :test_execution
end
