class TestStep < ActiveRecord::Base
  belongs_to  :test_case
  has_many    :test_results
end
