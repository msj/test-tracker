class TestStepsController < ApplicationController
  before_action :set_test_step, only: [:show, :edit, :update, :destroy]

  # GET /test_steps
  # GET /test_steps.json
  def index
    @test_steps = TestStep.all
  end

  # GET /test_steps/1
  # GET /test_steps/1.json
  def show
  end

  # GET /test_steps/new
  def new
    @test_step = TestStep.new
  end

  # GET /test_steps/1/edit
  def edit
  end

  # POST /test_steps
  # POST /test_steps.json
  def create
    @test_step = TestStep.new(test_step_params)

    respond_to do |format|
      if @test_step.save
        format.html { redirect_to @test_step, notice: 'Test step was successfully created.' }
        format.json { render action: 'show', status: :created, location: @test_step }
      else
        format.html { render action: 'new' }
        format.json { render json: @test_step.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_steps/1
  # PATCH/PUT /test_steps/1.json
  def update
    respond_to do |format|
      if @test_step.update(test_step_params)
        format.html { redirect_to @test_step, notice: 'Test step was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @test_step.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_steps/1
  # DELETE /test_steps/1.json
  def destroy
    @test_step.destroy
    respond_to do |format|
      format.html { redirect_to test_steps_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_step
      @test_step = TestStep.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_step_params
      params[:test_step]
    end
end
