class CreateTestExecutions < ActiveRecord::Migration
  def change
    create_table :test_executions do |t|
      t.date :date_executed

      t.timestamps
    end
  end
end
