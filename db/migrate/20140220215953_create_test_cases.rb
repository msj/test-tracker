class CreateTestCases < ActiveRecord::Migration
  def change
    create_table :test_cases do |t|
      t.belongs_to :use_case
      t.string :case_id
      t.string :case_title

      t.timestamps
    end
  end
end
