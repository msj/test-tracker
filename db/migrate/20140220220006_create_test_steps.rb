class CreateTestSteps < ActiveRecord::Migration
  def change
    create_table :test_steps do |t|
      t.belongs_to :test_case
      t.string :step_name
      t.timestamps
    end
  end
end
