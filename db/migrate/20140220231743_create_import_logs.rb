class CreateImportLogs < ActiveRecord::Migration
  def change
    create_table :import_logs do |t|
      t.datetime :import_date
      t.string :file_name
      t.integer :file_size

      t.timestamps
    end
  end
end
