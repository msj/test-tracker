# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140220231743) do

  create_table "import_logs", force: true do |t|
    t.datetime "import_date"
    t.string   "file_name"
    t.integer  "file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "test_cases", force: true do |t|
    t.integer  "use_case_id"
    t.string   "case_id"
    t.string   "case_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "test_executions", force: true do |t|
    t.date     "date_executed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "test_results", force: true do |t|
    t.integer  "test_execution_id"
    t.integer  "test_step_id"
    t.boolean  "status"
    t.string   "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "test_steps", force: true do |t|
    t.integer  "test_case_id"
    t.string   "step_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "use_cases", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
